# Tracker
Author:&ensp;&ensp;&ensp;[Tomasz Zięba][1] <tomasz.zieba.71@gmail.com>

## Description
Simple website that allows browsing / searching movies from database provided by http://www.omdbapi.com/  

## Requirements

### Data presentation
API results should be viewable on a website with basic layout, with pagination
and a possibility to save favourites into local database.

### Access to data
Only logged in users are capable to use this feature (standard login or Facebook/Google).

### Technologies
Whatever would be best for task realization (including JS frameworks).

### Publication
Entire project should be available as an open source project on GitHub.
The project should contain README file with information how to install application in local environment.

### Version control system
Please commit your work on a regular basis (rather then one huge commit).

### Deploying
Please use Docker for orchestration (docker-compose).

[1]: https://stackoverflow.com/users/3081328/hunter-71
