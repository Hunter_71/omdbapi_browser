from django.shortcuts import render

from browser.models import SearchedPhrase
from browser.omdbapi import OMDBAPI
from browser.pagination import Pagination


def index(request, search_text='', page=1):
    if request.method == 'POST':
        search_text = request.POST['search_text'].strip().lower()
        page = 1

    if search_text:
        SearchedPhrase.register(request.user, search_text)

        movies = OMDBAPI.search(search_text, page=page)
        pagination = Pagination(search_text, page, number=10, validator=OMDBAPI.has_page)
    else:
        movies = []
        pagination = None

    args = {
        'movies': movies,
        'search_text': search_text,
        'page': page,
        'pagination': pagination,
        'user': request.user,
    }
    return render(request, 'index.html', args)


def page(request):
    text = request.GET['search_text'].lower()
    page = int(request.GET['page'].lower())

    return index(request, text, page)
