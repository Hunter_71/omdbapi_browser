from django.conf.locale.en import formats as en_formats
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from browser.models import User, SearchedPhrase


en_formats.DATETIME_FORMAT = "Y-m-d H:i:s"


class SearchedPhraseAdmin(admin.ModelAdmin):
    list_display = (
        'user',
        'text',
        'timestamp',
    )
    list_filter = (
        'user',
    )
    readonly_fields = (
        'user',
        'text',
        'timestamp',
    )

    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


admin.site.register(SearchedPhrase, SearchedPhraseAdmin)
admin.site.register(User, UserAdmin)
