import requests

from movie_database.settings import OMDBAPI_KEY


class OMDBAPI(object):
    URL = 'http://www.omdbapi.com/'

    @staticmethod
    def map_params(kwargs):
        args = {
            'text': 's',
            'year': 'y',
            'page': 'page',
            'type': 'type',
        }
        params = {
            args[k]: v for k, v in kwargs.items() if k in args
        }
        params['apikey'] = OMDBAPI_KEY

        return params

    @staticmethod
    def get_json(kwargs):
        params = OMDBAPI.map_params(kwargs)
        return requests.get(OMDBAPI.URL, params=params).json()

    @staticmethod
    def get_data(kwargs):
        json_result = OMDBAPI.get_json(kwargs)

        if OMDBAPI.is_correct(json_result):
            return json_result['Search']

        return []

    @staticmethod
    def is_correct(json_result):
        return json_result['Response'] == 'True'

    @staticmethod
    def has_page(text, page):
        args = {
            'text': text,
            'page': page,
        }
        return OMDBAPI.is_correct(OMDBAPI.get_json(args))

    @staticmethod
    def search(text, **kwargs):
        kwargs['text'] = text
        return OMDBAPI.get_data(kwargs)
