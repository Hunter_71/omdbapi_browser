class Pagination(object):
    TRUE_VALIDATOR = lambda x, y: True

    def __init__(self, phrase, current, number=10, validator=TRUE_VALIDATOR):
        start = max(1, current - number // 2 + 1)
        stop = max(10, current + number // 2)

        while stop > current:
            if validator(phrase, stop):
                break
            stop -= 1

        self.current = current
        self.range = range(start, max(current + 1, stop))
        self.previous_page = 1 if start > 1 else 0
        self.next_page = stop if stop > current else 0
