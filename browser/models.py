from django.conf import settings
from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils import timezone


class User(AbstractUser):
    pass


class SearchedPhrase(models.Model):
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    text = models.CharField(max_length=255)
    timestamp = models.DateTimeField(default=timezone.now)

    @staticmethod
    def register(user, text):
        if user.is_authenticated and SearchedPhrase.last_searched_text(user) != text:
            SearchedPhrase(user=user, text=text).save()

    @staticmethod
    def last_searched_text(user):
        last_phrase = SearchedPhrase.objects.filter(user=user).order_by('timestamp').last()
        return last_phrase.text if last_phrase else ''
